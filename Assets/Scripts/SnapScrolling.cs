﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class SnapScrolling : MonoBehaviour
{
    [Header("List of pans")] 
    [SerializeField]
    private List<GameObject> m_panPrefabs;

    public List<GameObject> PanPrefabs
    {
        get => m_panPrefabs;
        set => m_panPrefabs = value;
    }

    [Tooltip("Current pan ID")]
    [SerializeField] 
    private int m_SelectedPanID;
    
    public int SelectedPanId
    {
        get => m_SelectedPanID;
        set => m_SelectedPanID = value;
    }

    [Tooltip("Current count of pan list")] 
    private int m_Count;
    
    public int Count
    {
        get => m_Count;
        set => m_Count = value;
    }

    [Tooltip("Max scale of pans")]
    [Range(0.1f, 20f)] 
    [SerializeField] private float maxScale;

    [SerializeField] private int panOffset;

    [SerializeField] private float snapSpeed;

    [SerializeField] private float scaleSpeed;

    [SerializeField] private float scaleOffset;

    private int panCount;
    
    private GameObject[] panList;
    private Vector2[] panPos;
    private Vector2[] panScale;
    private Vector2 contentVector;
    private RectTransform contentRect;

    private bool isScrolling;

    public void GetButton()
    {
        m_SelectedPanID = 0;
        
        if (m_Count > 0)
        {
            contentRect = GetComponent<RectTransform>();
            panList = new GameObject[m_Count];
            panPos = new Vector2[m_Count];
            panScale = new Vector2[m_Count];

            var offsetX = m_panPrefabs[0].GetComponent<RectTransform>().sizeDelta.x;

            for (int i = 0; i < m_Count; i++)
            {
                panList[i] = m_panPrefabs[i];
                
                if(i == 0) continue;
                panList[i].transform.localPosition = new Vector2(panList[i - 1].transform.localPosition.x + offsetX + panOffset,
                    panList[i].transform.localPosition.y);

                panPos[i] = -panList[i].transform.localPosition;
            }
        }
    }

    private void FixedUpdate()
    {
        if (m_Count > 0)
        {
            var iMinus = new Vector2();
            var iMajor = new Vector2();

            if (contentRect.anchoredPosition.x <= panPos[0].x ||
                contentRect.anchoredPosition.x >= panPos[panPos.Length - 1].x)
            {
                isScrolling = false;
            }

            var nearestPos = float.MaxValue;
            for (int i = 0; i < m_Count; i++)
            {
                var distance = Mathf.Abs(contentRect.anchoredPosition.x - panPos[i].x);
                if (distance < nearestPos)
                {
                    nearestPos = distance;
                    m_SelectedPanID = i;
                }

                Scale(distance, i, 1f);
            }

            if (isScrolling) return;
            
            contentVector.x = Mathf.SmoothStep(contentRect.anchoredPosition.x, panPos[m_SelectedPanID].x,
                snapSpeed * Time.deltaTime);
            contentRect.anchoredPosition = contentVector;
        }
    }

    private void Update()
    {
        if (m_Count > 0)
        {
            for (int i = 0; i < m_Count; i++)
            {
                if (i != m_SelectedPanID)
                    panList[i].GetComponent<Button>().enabled = false;
                else
                    panList[i].GetComponent<Button>().enabled = true;
            }
        }
    }

    private void Scale(float distance, int i, float offset)
    {
        var scale = Mathf.Clamp(1 / (distance / panOffset) * scaleOffset * offset, 0.1f, maxScale);
        panScale[i].x = Mathf.SmoothStep(panList[i].transform.localScale.x, scale + 0.3f, scaleSpeed * Time.fixedDeltaTime);
        panScale[i].y = Mathf.SmoothStep(panList[i].transform.localScale.y, scale + 0.3f, scaleSpeed * Time.fixedDeltaTime);
        panList[i].transform.localScale = panScale[i];
    }

    public void Scrolling(bool scroll)
    {
        isScrolling = scroll;
    }

}
