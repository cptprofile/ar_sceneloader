﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.Networking;

public class ServerAPI : MonoBehaviour
{
    private const string BundleFolder = "https://argreality.com/Unity/AssetBundles/";
    private const string ItemList = "https://argreality.com/Unity/ItemList.php";

    public void GetItemList(UnityAction<List<string>> callback)
    {
        StartCoroutine(GetItemListRoutine(callback));
    }

    IEnumerator GetItemListRoutine(UnityAction<List<string>> callback)
    {
        UnityWebRequest www = UnityWebRequest.Get(ItemList);
        yield return www.SendWebRequest();

        if (www.isNetworkError)
        {
            Debug.Log("Network error");
        }
        else
        {
            string rawText = www.downloadHandler.text;

            string[] items = rawText.Split(',');

            List<string> itemList = items.Where(x => !string.IsNullOrEmpty(x)).ToList();

            callback.Invoke(itemList);
        }
    }

    public void GetBundleObject(string assetName, UnityAction<GameObject> callback, Transform bundleParent)
    {
        StartCoroutine(GetDisplayBundleRoutine(assetName, callback, bundleParent));
    }

    IEnumerator GetDisplayBundleRoutine(string assetName, UnityAction<GameObject> callback, Transform bundleParent)
    {
        string bundleURL = BundleFolder + assetName + "-";
        
#if UNITY_ANDROID
        bundleURL += "Android" + "?raw=true";
#else
        bundleURL += "IOS" + "?raw=true";
#endif

        Debug.Log("Requesting bundle at " + bundleURL);

        UnityWebRequest www = UnityWebRequestAssetBundle.GetAssetBundle(bundleURL);
        yield return www.SendWebRequest();

        if (www.isNetworkError)
        {
            Debug.Log("Network error");
        }
        else
        {
            AssetBundle bundle = DownloadHandlerAssetBundle.GetContent(www);
            if (bundle != null)
            {
                string rootAssetPath = bundle.GetAllAssetNames()[0];
                var arGameObject = Instantiate(bundle.LoadAsset(rootAssetPath) as GameObject, bundleParent);
                
                bundle.Unload(false);
                callback(arGameObject);
            }
            else
            {
                Debug.Log("Not a valid asset bundle");
            }
        }
    }
}
