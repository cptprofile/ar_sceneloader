﻿using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class ButtonInfoWriter : MonoBehaviour
{
    [SerializeField]
    private TextMeshProUGUI buttonText;
    private ContentController contentController;
    
    private void Start()
    {
        contentController = GameObject.FindObjectOfType<ContentController>();
        var uiButton = GetComponent<Button>();
        uiButton.onClick.AddListener(SetEvent);
    }

    private void SetEvent()
    {
        contentController.LoadContent(buttonText.text);
    }

    public void Init(string name)
    {
        buttonText.text = name;
        gameObject.name = name;
    }
}
