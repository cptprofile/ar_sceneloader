﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ContentController : MonoBehaviour
{
    [SerializeField]
    private ServerAPI m_Api;

    public ServerAPI Api
    {
        get => m_Api;
        set => m_Api = value;
    }

    public void LoadContent(string name)
    {
        DestroyAllChildren();
        m_Api.GetBundleObject(name, OnContentLoaded, transform);
    }

    private void OnContentLoaded(GameObject content)
    {
        Debug.Log("Loaded: " + content.name);
    }

    private void DestroyAllChildren()
    {
        foreach (Transform child in transform)
        {
            Destroy(child.gameObject);
        }
    }
}
