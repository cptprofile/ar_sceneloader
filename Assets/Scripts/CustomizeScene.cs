using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.XR.ARFoundation;

public class CustomizeScene : MonoBehaviour
{
    [SerializeField] 
    private ARPlaneManager arPlaneManager;

    [SerializeField] 
    private TextMeshProUGUI textInfo;
    
    public void Button_TogglePlaneDetection()
    {
        arPlaneManager.enabled = !arPlaneManager.enabled;

        if (arPlaneManager.enabled)
        {
            textInfo.text = "Off";
            SetAllPlanesActive(true);
            
        } else
        {
            textInfo.text = "On";
            SetAllPlanesActive(false);
        }
    }

    private void SetAllPlanesActive(bool status)
    {
        foreach (var plane in arPlaneManager.trackables)
        {
            plane.gameObject.SetActive(status);
        }
    }
}
