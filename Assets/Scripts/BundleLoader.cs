﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

public class BundleLoader : MonoBehaviour
{
    [SerializeField]
    private ServerAPI m_Api;

    public ServerAPI Api
    {
        get => m_Api;
        set => m_Api = value;
    }

    [SerializeField] 
    private ContentController m_ContentController;

    public ContentController ContentController
    {
        get => m_ContentController;
        set => m_ContentController = value;
    }

    [SerializeField] 
    private SnapScrolling m_SnapScrolling;

    public SnapScrolling SnapScrolling
    {
        get => m_SnapScrolling;
        set => m_SnapScrolling = value;
    }

    [SerializeField] 
    private GameObject m_ButtonPrefab;

    public GameObject ButtonPrefab
    {
        get => m_ButtonPrefab;
        set => m_ButtonPrefab = value;
    }

    [SerializeField] 
    private Transform m_ButtonParent;

    public Transform ButtonParent
    {
        get => m_ButtonParent;
        set => m_ButtonParent = value;
    }

    [Tooltip("Current loaded bundle id")] 
    [SerializeField]
    private int m_CurrentID;

    public int CurrentId
    {
        get => m_CurrentID;
        set => m_CurrentID = value;
    }

    private GameObject spawnedObject;
    private List<string> currentItemList;

    private void Start()
    {
        LoadMenuItems();
        m_CurrentID = -1;
    }

#if UNITY_EDITOR
    private void Update()
    {
        if (Input.GetKeyDown(KeyCode.Q))
        {
            LoadMenu();
        }
    }
#endif

    public void ActivateMenu(bool active)
    {
        if (active)
            LoadMenuItems();
        
        m_SnapScrolling.gameObject.SetActive(active);
    }

    private void DestroyAllButtons()
    {
        foreach (Transform child in m_ButtonParent)
        {
            Destroy(child.gameObject);
        }
    }

    private void LoadMenuItems()
    {
        // DestroyAllButtons();
        m_Api.GetItemList(OnMenuItemsLoaded);
    }

    public void OnMenuItemsLoaded(List<string> itemList)
    {
        currentItemList = itemList;
        m_SnapScrolling.PanPrefabs.Clear();
        foreach (var item in itemList)
        {
            GameObject buttonObject = Instantiate(m_ButtonPrefab, m_ButtonParent);
            m_SnapScrolling.PanPrefabs.Add(buttonObject);

            var buttonInfoWriter = buttonObject.GetComponent<ButtonInfoWriter>();
            buttonInfoWriter.Init(item);
        }
        
        m_SnapScrolling.Count = itemList.Count;
        m_SnapScrolling.GetButton();
    }

    private void LoadContent(string item)
    {
        m_ContentController.LoadContent(item);
    }

    public bool LoadMenu()
    {
        if (m_SnapScrolling.PanPrefabs.Count > 0)
        {
            if(m_CurrentID != m_SnapScrolling.SelectedPanId || m_CurrentID < 0)
            {
                var item = currentItemList[m_SnapScrolling.SelectedPanId];
            
                m_ContentController.LoadContent(item);
                m_CurrentID = m_SnapScrolling.SelectedPanId;

                return true;
            }
        }

        return false;
    }
}
